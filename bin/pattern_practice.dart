import 'package:pattern_practice/pattern_practice.dart' as pattern_practice;

void main(List<String> arguments) {
  var singl = Singletone();
  var singl2 = Singletone();

  User user = User('Egor', '223', '@mail', '185');
  user = user.copywith(name: 'Vadim', phone: '8755');
  User coolUser = User.buildCoolUser();
  User classicUser = User.buildClassicUser();
  // Machine machine = Machine(user.phone, user.id); Not valid
  Machine machine = Adapter.adaptee(user);
  print(machine.id);
  print(user.toStringUser());
  print(coolUser.toStringUser());
  print(classicUser.toStringUser());
  coolUser.language(LanguageStrategyA());
  classicUser.language(LanguageStrategyB());

  classicUser.say();
  classicUser.isAngry = true;
  classicUser.say();
}

class Singletone {
  static final Singletone _singletone = Singletone._();
  Singletone._();
  factory Singletone() {
    return _singletone;
  }
}

class User {
  final String name;
  final String id;
  final String email;
  final String phone;
  bool isAngry = false;

  User(this.name, this.id, this.email, this.phone);
  User copywith({String name, String id, String email, String phone}) => User(
        name = name ?? this.name,
        id = id ?? this.id,
        email = email ?? this.email,
        phone = phone ?? this.phone,
      );
  factory User.buildCoolUser() {
    return User('Cool Vadim', '228', '@coolmail.com', '777');
  }

  factory User.buildClassicUser() {
    return User('Ivan', '034', '@mail.com', '569');
  }
  String toStringUser() {
    return '$name $id $phone $email';
  }

  void say() {
    !isAngry ? print('All cool') : print('I am angry');
  }

  void language(IStrategy strategy) {
    strategy.execute();
  }
}

abstract class Adapter {
  static Machine adaptee(User user) {
    return Machine(
      int.parse(user.phone),
      int.parse(user.id),
    );
  }
}

class Machine {
  final int phone;
  final int id;

  Machine(this.phone, this.id);
}

abstract class IStrategy {
  void execute();
}

class LanguageStrategyA implements IStrategy {
  @override
  void execute() {
    print('Hello');
  }
}

class LanguageStrategyB implements IStrategy {
  @override
  void execute() {
    print('Aloha');
  }
}
